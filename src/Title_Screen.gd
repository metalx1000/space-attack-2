extends Node2D

onready var play_btn = $Buttons/Play_Btn
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$player/AnimationPlayer.play("ship")
	play_btn.grab_focus()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$stars.rotation += .1 * delta

func _input(event):
	if Input.is_action_just_pressed("esc"):
		get_tree().quit()

func _on_Play_Btn_button_up():
	get_tree().change_scene("res://World.tscn")

func _on_Credits_Btn_button_up():
	get_tree().change_scene("res://Credits.tscn")


func _on_Settings_Btn_button_up():
	get_tree().change_scene("res://Settings.tscn")


func _on_Licenses_Btn_button_up():
	get_tree().change_scene("res://Licenses.tscn")
	


func _on_Exit_Btn_button_up():
	get_tree().quit()
	pass # Replace with function body.


func _on_Timer_timeout():
	get_tree().change_scene("res://Story.tscn")


func _on_Story_Btn_button_up():
	get_tree().change_scene("res://Story.tscn")


func _on_Continue_Btn_button_up():
	Main.load_settings()
	get_tree().change_scene("res://World.tscn")
	pass # Replace with function body.
