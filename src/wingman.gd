extends Sprite
var plLaser = preload("res://Lasers.tscn")

var speed = 200
var timer = Timer.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	start_timer()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.y -= speed * delta
	if position.y < 0:
		queue_free()

func start_timer():
	timer.connect("timeout",self,"create_laser") 
	timer.wait_time = rand_range(.5,1)
	add_child(timer) #to process
	timer.start() #to start

func create_laser():
	var laser = plLaser.instance()
	laser.get_node("laser").add_to_group("player_laser")
	laser.speed = 50
	laser.position = position
	get_tree().current_scene.add_child(laser)
