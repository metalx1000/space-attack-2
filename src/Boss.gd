extends Sprite

var life=100

var pos_y_min = -250
var pos_y_max = -150
var laser_pos_y = 200

var plLaser = preload("res://Lasers.tscn")
var plExplosion = preload("res://Explosions.tscn")


var speed_x = 2
var speed_y = 2
#activate if kills is greater than 'active' amount
var active = 350
var start = 0
var level = 2
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	visible = true
	pass # Replace with function body.

func _process(delta):
	
	if life < 1:
		death_explode()
		if start == 1:
			start = 2
			death()
			
	if Main.kills >= active && Main.level == level:
		Main.boss_life = life
		movement()
		if start == 0:
			start = 1
			Main.change_music("res://res/music/boss_1.ogg")
			yield(get_tree().create_timer(3), "timeout")
			Main.HUD("boss",.6)
			$Timer.start()
			
		
func create_laser():
	var laser = plLaser.instance()
	laser.get_node("laser").add_to_group("enemy_laser")
	#make laser red
	laser.modulate.r = 1
	laser.modulate.g = 0
	laser.modulate.b = 0
	laser.modulate.a = 1
	
	laser.scale = Vector2(6,6)
	laser.rotation_degrees = 180
	laser.speed = -600
	laser.position.x = position.x
	laser.position.y = position.y + laser_pos_y
	get_tree().current_scene.add_child(laser)
	
func movement():
	if position.y < pos_y_min:
		speed_y = rand_range(.2,1)
	elif position.y > pos_y_max:
		speed_y = rand_range(-.1,-.2)
		
	if position.x < get_viewport_rect().size.x *.25:
		speed_x = rand_range(.5,4)
	elif position.x > get_viewport_rect().size.x *.75:
		speed_x = rand_range(-.5,-4)
	
	position.y += speed_y
	position.x += speed_x
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func explode(scale,pos):
	var explosion = plExplosion.instance()
	explosion.position = pos
	explosion.scale = Vector2(scale,scale)
	if rand_range(0,3) > 1:
		explosion.get_node("explode_snd").autoplay = false
	
	get_tree().current_scene.add_child(explosion)
	explosion.get_node("Explosion").play()
	
func death():
	Main.boss_life = 0
	Main.level += 1
	Main.kills += 1 
	Main.score += 100
	Main.change_music("res://res/music/music_" + str(Main.level) +".ogg")
	Main.HUD("boss_death",.6)
	yield(get_tree().create_timer(2), "timeout")
	var pos = Vector2(get_viewport_rect().size.x/2,150)
	explode(15,pos)
	yield(get_tree().create_timer(1), "timeout")
	pos = Vector2(get_viewport_rect().size.x/2,250)
	explode(10,pos)
	queue_free()

func death_explode():
	var x = rand_range(0,get_viewport_rect().size.x)
	var y = rand_range(0,300)
	var scale = rand_range(1,2)
	var pos = Vector2(x,y)
	explode(scale,pos)	
	pass
	


func _on_Area2D_body_entered(body):
	if Main.kills >= active && Main.level == level:
		if body.is_in_group("player_laser"):
			explode(.5,body.get_parent().position)
			Main.score += 10
			var laser = body.get_parent()
			if laser.life == 0:
				laser.death()
			life -= laser.damage
		
		elif body.is_in_group("Players") and body.DEAD == false:
			body.death()
		


func _on_Timer_timeout():
	if Main.kills >= active:
		if rand_range(0,2) < 1:
			create_laser()
