extends Node2D


var speed_y = 300
var speed_x = 0

var life = 1
#bullet direction
var plLaser = preload("res://Lasers.tscn")
var plExplosion = preload("res://Explosions.tscn")

	
func _physics_process(delta):
	if life < 1:
		death()
	position.y += speed_y * delta 
	position.x += speed_x * delta + (Main.star_speed_x/ 50)
	if position.y < -128 || position.y > get_viewport_rect().size.y + 128:
		queue_free()


func _on_VisibilityNotifier2D_screen_exited():
		queue_free()

func create_laser(diagonal,speed):
		#add randomness to laser timing
		yield(get_tree().create_timer(rand_range(0,1)), "timeout")
		var laser = plLaser.instance()
		laser.get_node("laser").add_to_group("enemy_laser")
		#make laser red
		laser.scale = Vector2(2,2)
		laser.modulate.r = 1
		laser.modulate.g = 0
		laser.modulate.b = 0
		laser.modulate.a = 1
		
		laser.diagonal = diagonal
		laser.speed = speed
		laser.position = position
		get_tree().current_scene.add_child(laser)

func death():
	var explosion = plExplosion.instance()
	explosion.position = position
	get_tree().current_scene.add_child(explosion)
	explosion.get_node("Explosion").play()
	Main.kills += 1 
	Main.score += 10
	if Main.kills % 100 == 0:
		Main.score += Main.kills
		Main.HUD("kill_bonus",.7)
	queue_free()

func _on_Area2D_body_entered(body):
	if body.is_in_group("player_laser"):
		Main.score += 10
		var laser = body.get_parent()
		if laser.life == 0:
			laser.death()
		life -= laser.damage
		
	elif body.is_in_group("Players") and body.DEAD == false:
		death()
		body.death()
		
	


func _on_Death_Timer_timeout():
	death()
	#VisualServer.set_default_clear_color(Color(0,0,0,1.0))
		
	pass # Replace with function body.


func _on_Life_Timer_timeout():
	queue_free()
	pass # Replace with function body.


func _on_Timer_shoot():
	var val = rand_range(0, 3)
	if val <= 1 and position.y > 0 and position.y < get_viewport_rect().size.y:
		create_laser(0,-600)
	pass # Replace with function body.


func _on_onscreen_screen_exited():
	queue_free()
	pass # Replace with function body.
