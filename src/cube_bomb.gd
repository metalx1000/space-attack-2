extends Node2D
var plExplosion = preload("res://Explosions.tscn")

var DEAD = false
var turn_count = 0
var turn_wait = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	turn()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func death():
	DEAD = true
	var explosion = plExplosion.instance()
	explosion.position = position
	get_tree().current_scene.add_child(explosion)
	explosion.get_node("Explosion").play()
	yield(get_tree().create_timer(.5),"timeout")
	queue_free()

func turn():
	$beep.play()
	rotation_degrees += 90
	
	if turn_count == 25:
		turn_wait=10
		death()
	if turn_count == 20:
		turn_wait = .125
	elif turn_count == 5:
		turn_wait = .5
	elif turn_count == 10:
		turn_wait = .25
		
	turn_count+=1
	#wait before looping
	yield(get_tree().create_timer(turn_wait),"timeout")
	turn()


func hit(body):
	if DEAD == false:
		if body.is_in_group("Players") and body.DEAD == false:
			body.death()
			death()
		elif body.is_in_group("player_laser"):
			death()
			body.queue_free()
