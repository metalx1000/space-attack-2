extends Node2D

var speed = 600
var diagonal = 0
var damage = 1
var life = 0
var sound = "res://res/sounds/laser1.ogg"
var stream_player = AudioStreamPlayer2D.new()
var snd = true
#bullet direction
func _ready():
	if snd:
		play_sound()
	pass

func play_sound():
	#yield(get_tree().create_timer(1), "timeout")
		
	stream_player.stream = Main.laser_snd
	stream_player.stream.set_loop(false)
	
	stream_player.connect("finished", stream_player, "queue_free")
	add_child(stream_player)
	stream_player.play()


func _physics_process(delta):
	position.y -= speed * delta 
	position.x += diagonal

func death():
	queue_free()


func _on_onscrenn_screen_exited():
	death()
