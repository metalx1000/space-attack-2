extends Node2D

var plLaser = preload("res://Lasers.tscn")
onready var animation = $AnimationPlayer
onready var ship_1 = $Sprite
onready var ship_2 = $Sprite2

# Called when the node enters the scene tree for the first time.
func _ready():
	animation.play("movement")
	pass # Replace with function body.


func create_laser(diagonal,lspeed,rotation,snd,pos):
	var laser = plLaser.instance()
	laser.snd = snd
	laser.get_node("laser").add_to_group("player_laser")
	laser.diagonal = diagonal
	laser.speed = lspeed
	laser.rotation_degrees = rotation
	laser.position = pos
	get_tree().current_scene.add_child(laser)

func _on_Timer_timeout():
	var pos = ship_1.position	
	create_laser(20,0,-90,true,pos)
	pos = ship_2.position	
	create_laser(-20,0,-90,false,pos)
	pass # Replace with function body.


func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()
	pass # Replace with function body.
