extends Label


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	score = Main.score
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if score < Main.score:
		if score % 20 == 0:
			$coin_snd.play()
		score+=10
	
	text="Score: " + str(score)
	text+="\nDeaths: " + str(Main.deaths)
	text+="\nKills: " + str(Main.kills)
	text+="\nLevel: " + str(Main.level)
	
	if Main.boss_life > 0:
		text+="\nBoss: " + str(Main.boss_life)
#	pass
