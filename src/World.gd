extends Node2D

var enemies = preload("res://Enemies.tscn")
var powerups = preload("res://Powerups.tscn") 
var cube_scene = preload("res://cube.tscn") 

onready var HUD = get_node("HUD")

func _ready():
	
	Main.load_music("res://res/music/music_1.ogg")
	pass

func fade_scene():
	var a = 1.0
	while a > 0:
		modulate.a = a		
		$Background.get_children()[0].modulate.a = a
		a-=.01
		yield(get_tree().create_timer(.01),"timeout")
	
	get_tree().change_scene("res://Final.tscn")
	
# Called when the node enters the scene tree for the first time.
func _process(delta):
	if Main.fade_scene == true:
		Main.fade_scene = false
		fade_scene()
		
	#load cube final boss
	if Main.level == 5 && Main.kills > 3000 && Main.cube_activate:
		Main.cube_activate = false
		var cube = cube_scene.instance()
		get_tree().current_scene.add_child(cube)
		
	if Main.plus:
		Main.plus = false
		create_powerup()
		create_powerup()
		create_powerup()
		
	#if Input.is_action_just_pressed("ui_down"):
		#fade_scene()
	#	create_powerup()
		
		
	if Main.HUD_active:
		Main.HUD_active = false
		HUD.load_tween()
	
	pass # Replace with function body.

	
func create_enemy(type):
	if Main.enemies_active:
		var enemy = enemies.instance()
		var sprite = enemy.get_node("enemy/Sprite")
		sprite.play(type)
		#sprite.play("red")
		enemy.speed_y = rand_range(100, 300)
		if type == "blue":
			enemy.life = 3
			enemy.position.x = rand_range(64, get_viewport_rect().size.x - 64)
			enemy.position.y = -64
		if type == "green":
			enemy.life = 2
			var r = rand_range(0,2) 
			if r > 1:
				enemy.position.x = -64
				enemy.speed_x = rand_range(100,200)
			else:
				enemy.position.x = get_viewport_rect().size.x + 64
				enemy.speed_x = rand_range(-100,-200)
			enemy.position.y = rand_range(-128, get_viewport_rect().size.y/3)
		elif type == "yellow":
			enemy.position.x = rand_range(64, get_viewport_rect().size.x - 64)
			enemy.position.y = get_viewport_rect().size.y + 64 
			enemy.speed_y = rand_range(-300,-500)
		else:
			enemy.position.x = rand_range(64, get_viewport_rect().size.x - 64)
			enemy.position.y = -64
			
		get_tree().current_scene.add_child(enemy)

func create_powerup():
	var item = powerups.instance()
	item.position.y = rand_range(-128, get_viewport_rect().size.y/3)
	var r = rand_range(0,2) 
	if r > 1:
		item.position.x = -64
		item.speed_x = rand_range(1,2)
	else:
		item.position.x = get_viewport_rect().size.x + 64
		item.speed_x = rand_range(-1,-2)
		
	item.speed_y = rand_range(1,2)
	
	get_tree().current_scene.add_child(item)
	


func _on_Timer_timeout():
	create_enemy("red")
	
	if Main.level > 1 && rand_range(0,2) > 1:
		create_enemy("green")
	
	if Main.level > 2 && rand_range(0,2) > 1:
		create_enemy("blue")
		
	if Main.level > 3 && rand_range(0,3) > 2:
		create_enemy("yellow")
		
	if rand_range(0,5) < 1:
		create_powerup()
	
func _on_Exit_Btn_button_up():
	get_tree().change_scene("res://Title_Screen.tscn")


func _on_AutoSave_timeout():
	Main.save()
