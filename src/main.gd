extends Node

var deaths = 0
var kills = 0
var level = 1
var score = 0
var powerup = "none"
var rapid_fire = 0
var plus = false
var autofire = true

var laser_snd = preload("res://res/sounds/laser1.ogg")
var coin_snd = preload("res://res/sounds/coin10.ogg")

var config = ConfigFile.new()
var settings_file = "user://settings.cfg"

var star_speed_y = 300
var star_speed_x = 0
var player_position = Vector2(0,0)

var gamepad = false
var music_active = true
var music_fading = false
var music = AudioStreamPlayer.new()

var enemies_active = true
var cube_activate = true
var fade_scene = false

var boss_life = 0

var HUD_type = "loading"
var HUD_scale = 1
var HUD_active = false
var HUD_num = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _process(delta):
	
	pass
	
# Called when the node enters the scene tree for the first time.
func _ready():
	
	randomize()
	add_child(music)

	pass # Replace with function body.

func color_flash(time,count, color):
	for i in count:
		VisualServer.set_default_clear_color(color)
		yield(get_tree().create_timer(time), "timeout")
		VisualServer.set_default_clear_color(Color(0,0,0,1.0))
		yield(get_tree().create_timer(time), "timeout")


func HUD(type,scale):
	HUD_type = type
	HUD_scale = scale
	HUD_active = true
	if HUD_num == 0:
		HUD_num = 1
	else:
		HUD_num = 0


#Music Fade
func music_fade(direction):
	if !music_fading:
		music_fading = true
		var low_db = -80
		var high_db = -15
		if direction == "out":
			while music.volume_db > low_db:
				music.volume_db -= 1
				yield(get_tree().create_timer(.02), "timeout")
		elif direction == "in":
			while music.volume_db < high_db:
				music.volume_db += 1
				yield(get_tree().create_timer(.02), "timeout")
		
		music_fading = false
		
func change_music(res):
	var stream = load(res)
	music_fade("out")
	yield(get_tree().create_timer(3), "timeout")
	music.set_stream(stream)
	if music_active:
		music.play()
	
	music_fade("in")

	

func load_music(res):
	var stream = load(res)
	music.set_stream(stream)
	music.volume_db = -40
	music_fade("in")
	if music_active:
		music.play()
		

func mega_det():
	var enemies = get_tree().get_nodes_in_group("Enemies")
	for enemy in enemies:
		var timer = enemy.get_node("enemy/Death_Timer")
		timer.wait_time = rand_range(0,1)
		timer.start()

func save_settings(catagory,entry,value):
	config.set_value(catagory, entry, value)
	# Save the changes by overwriting the previous file
	config.save(settings_file)

func save():
	save_settings("game","level",level)
	save_settings("game","kills",kills)
	save_settings("game","deaths",deaths)
	save_settings("game","score",score)
	save_settings("game","autofire",autofire)
		
func load_settings():
	var err = config.load(settings_file)
	if err == OK: # If not, something went wrong with the file loading
		level = config.get_value("game","level",0)
		kills = config.get_value("game","kills",0)
		deaths = config.get_value("game","deaths",0)
		autofire = config.get_value("game","autofire",true)
		score = config.get_value("game","score",0)
		#check_points = config.get_value("maps","check_points","res://maps/e1m1.tscn")
		#difficulty = config.get_value("maps","difficulty",0)
		pass


	
	
	
	
