extends KinematicBody2D

var plLaser = preload("res://Lasers.tscn")
var plExplosion = preload("res://Explosions.tscn")
onready var animation = $AnimationPlayer

var DEAD = false
var invincible = false
var invincible_able = false
var speed = 500
var stop_distance = 5
var berserk_attack = 0
var berserk_speed = 0
var berserk_pos = Vector2(300,300)
var screen_size 
var type = null
var explosion_scale = 1
var tagalong_pos = Vector2(0,0)
var inv = 3
var follow_mouse = true

var auto_fire = 0.0
#var weapon = "default"


func _ready():
	screen_size = get_viewport_rect().size
	set_invicibility(3)
	

func _process(delta):
	update_position()

	#keep player in screen
	position.x = clamp(position.x, 32, screen_size.x - 32)
	position.y = clamp(position.y, 32, screen_size.y - 32)
	if follow_mouse:
		if Main.gamepad && type == "player":
			var gspeed = 10
			if Input.is_action_pressed("ui_left"):
				position.x -= gspeed;
			if Input.is_action_pressed("ui_right"):
				position.x += gspeed;
			
			if Input.is_action_pressed("ui_up"):
				position.y -= gspeed;
			if Input.is_action_pressed("ui_down"):
				position.y += gspeed;
		else:
			move_to_mouse()

		if Main.rapid_fire > 0 && type == "player":
			if DEAD == false and Main.rapid_fire % 6 == 0:
				create_laser(0,600,0,true)
			Main.rapid_fire -= 1
		
	#if Input.is_action_just_pressed("ui_down"):
		#berserk_attack = 200
		#Main.rapid_fire = 100
		
		if Input.is_action_pressed("ui_shoot") && DEAD == false && Main.autofire:
			auto_fire += delta
			if auto_fire > .2:
				auto_fire = 0
				shoot()
				
		if Input.is_action_just_pressed("ui_shoot") and DEAD == false:
			shoot()
			
func shoot():
	create_laser(0,600,0,true)
	if Main.powerup == "diagonal":
		create_laser(.5,600,0,false)
		create_laser(-.5,600,0,false)
	elif Main.powerup == "4way":
		create_laser(20,0,90,false)
		create_laser(-20,0,-90,false)
		create_laser(0,-600,180,false)
	elif Main.powerup == "rear_gun":
		create_laser(.5,-600,180,false)
		create_laser(-.5,-600,180,false)
			

func update_position():
	pass

func move_to_mouse():
	var pos = get_global_mouse_position()
	pos.y -= 64 #put player infront of pointer
	if type == "tagalong":
		pos = tagalong_pos
		if rand_range(0,10) < 1:
			tagalong_pos = Main.player_position
		
	if berserk_attack > 0:
		pos = berserk_attack(pos)
	else:
		berserk_speed = 1
		
	if position.distance_to(pos) > stop_distance:
		var move_pos = pos - position
		move_pos = move_pos.normalized()
		move_and_slide(move_pos * speed * berserk_speed)

func berserk_attack(pos):
	berserk_attack -= 1
	berserk_speed = 2
	
	if berserk_attack % 5 == 0:
		set_invicibility(5)
		berserk_pos.x = rand_range(0,get_viewport_rect().size.x)
		berserk_pos.y = rand_range(0,get_viewport_rect().size.y)
	pos = berserk_pos
	return pos
		
func create_laser(diagonal,lspeed,rotation,snd):
	if type == "tagalong":
		yield(get_tree().create_timer(rand_range(.1,1)), "timeout")
	var laser = plLaser.instance()
	laser.snd = snd
	laser.get_node("laser").add_to_group("player_laser")
	laser.diagonal = diagonal
	laser.speed = lspeed
	laser.rotation_degrees = rotation
	laser.position = position
	get_tree().current_scene.add_child(laser)

func death():
	
	if !invincible:
		follow_mouse = false
		for t in get_tree().get_nodes_in_group("Tagalong"):
			t.death()
		$death_snd.play()
		Main.rapid_fire = 0
		Main.powerup = "none"
		visible = false
		DEAD = true
		set_invicibility(2)
		$Death_Timer.start()
		Main.deaths+=1
		explode()

func quick_escape():
	follow_mouse = false
	Main.star_speed_x = -300
	Main.mega_det()
	set_invicibility(4)
	animation.play("escape")
	yield(get_tree().create_timer(4),"timeout")
	follow_mouse = true
	Main.mega_det()
	Main.star_speed_x = 0

	
func set_invicibility(timeout):
	if invincible_able:
		$Sprite.play("invincibility")
		#$CollisionShape2D.set_deferred("disabled", true)
		#$Area2D/HITBOX.set_deferred("disabled", true)
		invincible = true
		$Invin_Timer.wait_time = timeout
		$Invin_Timer.start()
	
func explode():
	var explosion = plExplosion.instance()
	explosion.position = position
	explosion.scale = Vector2(explosion_scale,explosion_scale)
	get_tree().current_scene.add_child(explosion)
	explosion.get_node("Explosion").play()
	
func _on_Area2D_body_entered(body):
	if body.is_in_group("enemy_laser"):
		body.get_parent().death()
		death()
		
#respawn
func _on_Death_Timer_timeout():
	DEAD = false
	position.x = screen_size.x/2
	position.y = screen_size.y/4*3
	visible = true
	follow_mouse = true
	set_invicibility(3)
	
	
func _on_Invin_timeout():
	$Sprite.play("ship")
	invincible = false
#	$CollisionShape2D.set_deferred("disabled", false)
#	$Area2D/HITBOX.set_deferred("disabled", false)
	pass # Replace with function body.
