extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if Main.music_active:
		$Music_Btn.modulate.a = 1
	else:
		$Music_Btn.modulate.a = .5
	
	if Main.autofire:
		$AutoFire.modulate.a = 1
	else:
		$AutoFire.modulate.a = .5
		


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Exit_Btn_button_up():
	get_tree().change_scene("res://Title_Screen.tscn")


func _on_Music_Btn_toggled(button_pressed):
	if !Main.music_fading:
		if button_pressed:
			$Music_Btn.modulate.a = .5
			Main.music_active = false
			#Main.music_fade("out")
			Main.music.volume_db = -80

		else:
			$Music_Btn.modulate.a = 1
			Main.music_active = true
			#Main.music_fade("in")
			Main.music.volume_db = -15

		


func _on_AutoFire_toggled(button_pressed):
		if Main.autofire:
			$AutoFire.modulate.a = .5
			Main.autofire = false
		else:
			$AutoFire.modulate.a = 1
			Main.autofire = true
