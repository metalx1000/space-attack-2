extends AnimatedSprite

onready var tween   = get_node("Tween") #the tween

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

	
func play_snd(res):
	if ResourceLoader.exists(res):
		var snd = AudioStreamPlayer.new()
		self.add_child(snd)
		snd.stream = Main.coin_snd
		snd.stream.set_loop(false)
		snd.play()
	else:
		print(res, " does not exist.")
		

func load_tween():
	var type = Main.HUD_type
	var snd = "res://res/sounds/hud/"+type+".ogg"
	print(snd)
	play_snd(snd)
	var scale = Main.HUD_scale
	
	play(type)
	
	if !tween.is_active():
		tween.interpolate_property(self, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 1, tween.TRANS_BACK, tween.EASE_IN_OUT)
		tween.interpolate_method(self, "set_scale", Vector2(0, 0), Vector2(scale, scale), 2, tween.TRANS_BACK, tween.EASE_IN_OUT)
		tween.interpolate_property(self, "scale", Vector2(scale, scale), Vector2(0, 0), 2, tween.TRANS_BACK, tween.EASE_IN_OUT, 2)
		tween.interpolate_property(self, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 2, tween.TRANS_BACK, tween.EASE_IN_OUT, 2)
		tween.start()
	
