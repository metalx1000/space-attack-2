extends "res://ship.gd"

func _ready():
	add_to_group("Players")
	explosion_scale = 2
	invincible_able = true
	DEAD = false
	invincible = false
	speed = 500
	stop_distance = 5
	berserk_attack = 0
	berserk_speed = 0
	berserk_pos = Vector2(300,300)
	screen_size 
	type = "player"

	inv = 3
#var weapon = "default"

func update_position():
	#if action button on gamepad press
	# use joystick and hide mouse
	if Input.is_action_pressed("game_pad"):
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		Main.gamepad = true
		
	Main.player_position = position

func _on_Area2D_body_entered(body):
	if body.is_in_group("enemy_laser"):
		body.get_parent().death()
		death()
